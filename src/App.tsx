import SignUp from '@pages/SignUp/SignUp';
import FormProvider from 'context';
import './App.scss';

function App(): any {
  return (
    <FormProvider>
      <SignUp />
    </FormProvider>
  );
}

export default App;
